# Typo found!

## Copy of current text
<!-- Copy the typo and the text around it.
Highlight the typo in **bold** -->

<blockquote>
... Put the text here ...
</blockquote>

## Corrected text
<!-- Same text as above but with typo corrected.
Highlight the correction in **bold** -->
<blockquote>
... Put the text here ...
</blockquote>

I Found this bug in: (Documentation on GitLab, Rust Docs, RapiDoc, Swagger-ui, GraphQL, code,...)
Link to page: (page where you found the typo)

### DF Storyteller version
Run `df_storyteller --version`:

<!-- Leave the information below untouched! -->
/label ~Typo ~Documentation
