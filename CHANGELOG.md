# Changelog
In this file you will find all changes to DF Storyteller.
These are changes to the whole application. 
For changes about the API, [see this document](CHANGELOG_API.md).

## Version 0.1.1 (2020-08-15)
- Disable colors and icons for Windows build in CI pipeline.
- df_st_api added database features filtering to solve [#75](https://gitlab.com/df_storyteller/df-storyteller/-/issues/75).
- Moved "delete database" message for Postgres to be shown at a better moment.
- Database schema: Switching from `Bytea` to `Binary` for images to be database agnostic.
- Guide: added build instructions for Linux.
- Guide: added favicon to main directory.
- Guide: added info for importing folders.
- API: added favicon to main directory.
- Import: Added note for long import time of `Historical Events`.
- Fixed Max OS compiler used for building from source.

## Version 0.1.0 (2020-08-14)
Initial public release. :D
Only took me 6 months :P

Greetings from the past if someone reads this in the future.
This application was founded during the (start of) COVID-19 (aka Coronavirus) pandemic.
Do you still remember that?