# Changelog API
In this file you will find all changes to DF Storytellers API.
These are only the changes to the API, not the application itself. 
For changes about the application itself, [see this document](CHANGELOG.md).


## Version 0.1.1 (2020-08-15)
No changes in API

## Version 0.1.0 (2020-08-14)
Initial public release.