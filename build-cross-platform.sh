#!/bin/sh

# Targets (all Tier 1 for Desktop/Laptops https://rust-lang.github.io/rustup-components-history/ )
# i686-pc-windows-gnu # Not supported by cross & PostgreSQL
# i686-pc-windows-msvc # Not supported by cross & PostgreSQL
# i686-unknown-linux-gnu # Not supported by PostgreSQL
# x86_64-apple-darwin # Not supported by cross
# x86_64-pc-windows-gnu
# x86_64-pc-windows-msvc # Not supported by cross
# x86_64-unknown-linux-gnu

# for supported targets with cross https://github.com/rust-embedded/cross#supported-targets
# x86_64-unknown-linux-gnu
# x86_64-pc-windows-gnu
# i686-unknown-linux-gnu

# PostgreSQL supports on the following architectures: (linux)
# amd64
# arm64 (Buster and newer)
# i386 (Buster and older)
# ppc64el

# Other cross compiler projects to take a look:
# https://github.com/mcandre/remy
# https://github.com/mcandre/tonixxx
# https://github.com/japaric/trust

# Other usefull links:
# https://doc.rust-lang.org/cargo/reference/build-scripts.html#overriding-build-scripts
# libpq https://github.com/sgrif/pq-sys/blob/master/build.rs

# https://github.com/burtonageo/cargo-bundle

# ------- Linux --------
# Build steps are easy.

# Build Docker containers locally
docker build -t dfstoryteller/builder:x86_64-unknown-linux-gnu-0.2.0 ./docker/x86_64-unknown-linux-gnu/

# Update Containers on Dockerhub
# docker push dfstoryteller/builder:x86_64-unknown-linux-gnu-0.2.0

# Run build process in containers
cross build --verbose --target x86_64-unknown-linux-gnu
# or
# cross build --target x86_64-unknown-linux-gnu --release

# ------- Windows --------
# Requirements:
# * Rust (rustup + cargo) (https://www.rust-lang.org/tools/install)
#   * `rustup toolchain install nightly`
# * PostgreSQL (minimum version unknown at the moment.) (https://www.postgresql.org/download/windows/)
#   * If you do not have PostgreSQL installed in the default directory you might
#     have to edit the Cargo.toml or .cargo/config file if you get a `libpq.lib` error.
# * Visual C++ (or have Visual Studio installed with Visual C++ option enabled.)
#   (https://visualstudio.microsoft.com/visual-cpp-build-tools/)
#   * Check 'C++ build tools' and click 'Install'.

# I got x86_64-pc-windows-msvc working in VM. Instructions added to README.md
# Because windows makes things difficult you have to take a few extra steps.
# (Some steps might be different depending on the language and version of your system.)
# Following steps are tested on Windows 10.
#
# (source of info: https://stackoverflow.com/a/20412645/2037998)
# * Open File Explorer
# * Right-click on "This PC" (in the left bar) and select "Properties"
# * In the new window click on "Advanced system settings"
# * In the new window click "Environment Variables..." (in the "Advanced" tab)
# * In the new window again, in the top section "User variables for <username>"
#   Dubble-click on "Path"
# * In that window click "New" and paste or type `C:\Program Files\PostgreSQL\12\bin`
#   and hit `Enter`.
# * Click "New" again and paste or type `C:\Program Files\PostgreSQL\12\lib`
#   and hit `Enter`.
# * Now click "OK" on the last 3 windows and you can close the others.
#
# Now windows knows where to find all the `.dll` files it needs to communicate with
# PostgreSQL.

# Docker compile on Linux for Windows
# Build Docker containers locally
docker build -t dfstoryteller/builder:x86_64-pc-windows-gnu-0.2.0 ./docker/x86_64-pc-windows-gnu/

# fixing current problem help:
# https://stackoverflow.com/questions/7469336/problem-with-library-libpq-in-debian

# Update Containers on Dockerhub
# docker push dfstoryteller/builder:x86_64-pc-windows-gnu-0.2.0

# Run build process in containers
cross build --verbose --target x86_64-pc-windows-gnu
# or
# cross build --target x86_64-pc-windows-gnu --release

# Maybe look at https://zork.net/~st/jottings/rust-windows-and-debian.html
# Windows installer:
# We might have to create a windows installer to make it easier for the user and
# set the environment variables. This also allows us to create shortcuts and
# open the terminal in the right path.
# Cargo-WIX: https://github.com/volks73/cargo-wix (but uses its own compiler :( )
# Cargo bundle: https://github.com/burtonageo/cargo-bundle

# ------- OSX --------
# https://www.reddit.com/r/rust/comments/6rxoty/tutorial_cross_compiling_from_linux_for_osx/
# TODO
