use cp437::convert_byte;
use std::io::Bytes;
use std::io::Read;
use std::iter::Iterator;

pub struct Reader<'a, R: 'a> {
    buffer: &'a mut Bytes<R>,
}

impl<'a, R: Read> Reader<'a, R> {
    pub fn new(buffer: &'a mut Bytes<R>) -> Self {
        Reader { buffer: buffer }
    }

    pub fn consume(&mut self, length: usize) -> String {
        let s: String = self
            .buffer
            .take(length)
            .filter_map(|x| match x {
                Ok(ref b) => Some(convert_byte(b)),
                Err(_e) => None,
            })
            .collect();
        s
    }
}
