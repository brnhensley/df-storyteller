use crate::ServerInfo;
use failure::Error;
use rocket::{get, State};
use rocket_contrib::json::Json;
use rocket_okapi_codegen::openapi;

/// Request information about DF Storyteller itself.
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/df_st_info")]
pub fn get_df_st_info(server_info: State<ServerInfo>) -> Result<Json<df_st_core::DFSTInfo>, Error> {
    Ok(Json(df_st_core::DFSTInfo {
        version: env!("CARGO_PKG_VERSION").to_owned(),
        enabled_features: Vec::new(),
        enabled_mods: Vec::new(),
        base_url: server_info.base_url.clone(),
        world_id: server_info.world_id,
    }))
}
