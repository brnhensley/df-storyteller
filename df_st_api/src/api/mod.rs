pub mod artifacts;
pub mod coordinates;
pub mod creatures;
pub mod dance_forms;
pub mod df_st_info;
pub mod entities;
pub mod entity_populations;
pub mod he_relationship_supplements;
pub mod he_relationships;
pub mod historical_eras;
pub mod historical_event_collections;
pub mod historical_events;
pub mod historical_figures;
pub mod identities;
pub mod landmasses;
pub mod map_images;
pub mod mountain_peaks;
pub mod musical_forms;
pub mod poetic_forms;
pub mod regions;
pub mod rivers;
pub mod site_map_images;
pub mod sites;
pub mod underground_regions;
pub mod world_constructions;
pub mod world_info;
pub mod written_content;

use okapi::openapi3::OpenApi;
use rocket::Route;
use rocket_okapi_codegen::routes_and_spec_with_openapi;

// The following variables are used by `rocket_okapi_codegen` to create the openAPI info
static APPLICATION_NAME: &'static str = "DF Storyteller";
static APPLICATION_DESCRIPTION: &'static str = "DF Storyteller is an application for parsing and \
storing Dwarf Fortress Legends files. It provides an interface for other apps to visualize the data.";
static APPLICATION_LICENSE: &'static str = "GPL-3.0-or-later";
static APPLICATION_LICENSE_URL: &'static str =
    "https://gitlab.com/df_storyteller/df-storyteller/-/blob/master/LICENSE";
static APPLICATION_DOCS_NAME: &'static str = "Rust Docs";
static APPLICATION_DOCS_URL: &'static str = "https://docs.dfstoryteller.com/rust-docs/";

fn get_spec_and_routes() -> (OpenApi, Vec<Route>) {
    let (spec, routes) = routes_and_spec_with_openapi![
        // World Info
        world_info::get_world_info,
        world_info::list_worlds_info,
        world_info::get_world_info_count,
        // Regions
        regions::get_region,
        regions::list_regions,
        regions::get_region_count,
        // Sites
        sites::get_site,
        sites::list_sites,
        sites::get_site_count,
        // Sites/Structures
        sites::get_site_structure,
        sites::list_site_structures,
        sites::get_site_structure_count,
        // Sites/SiteProperty
        sites::get_site_property,
        sites::list_site_properties,
        sites::get_site_property_count,
        // Structures
        sites::list_structures,
        sites::get_structure_count,
        // SiteProperty
        sites::list_all_site_properties,
        sites::get_all_site_property_count,
        // UndergroundRegions
        underground_regions::get_underground_region,
        underground_regions::list_underground_regions,
        underground_regions::get_underground_region_count,
        // Artifact
        artifacts::get_artifact,
        artifacts::list_artifacts,
        artifacts::get_artifact_count,
        // Coordinates
        coordinates::get_coordinate,
        coordinates::list_coordinates,
        coordinates::get_coordinate_count,
        // WorldConstruction
        world_constructions::get_world_construction,
        world_constructions::list_world_constructions,
        world_constructions::get_world_construction_count,
        // EntityPopulation
        entity_populations::get_entity_population,
        entity_populations::list_entity_populations,
        entity_populations::get_entity_population_count,
        // HistoricalEra
        historical_eras::get_historical_era,
        historical_eras::list_historical_eras,
        historical_eras::get_historical_era_count,
        // HistoricalEvent
        historical_events::get_historical_event,
        historical_events::list_historical_events,
        historical_events::get_historical_event_count,
        // HistoricalFigure
        historical_figures::get_historical_figure,
        historical_figures::list_historical_figures,
        historical_figures::get_historical_figure_count,
        // DanceForm
        dance_forms::get_dance_form,
        dance_forms::list_dance_forms,
        dance_forms::get_dance_form_count,
        // MusicalForm
        musical_forms::get_musical_form,
        musical_forms::list_musical_forms,
        musical_forms::get_musical_form_count,
        // PoeticForm
        poetic_forms::get_poetic_form,
        poetic_forms::list_poetic_forms,
        poetic_forms::get_poetic_form_count,
        // Identity
        identities::get_identity,
        identities::list_identities,
        identities::get_identity_count,
        // Landmass
        landmasses::get_landmass,
        landmasses::list_landmasses,
        landmasses::get_landmass_count,
        // Rivers
        rivers::get_river,
        rivers::list_rivers,
        rivers::get_river_count,
        // WrittenContent
        written_content::get_written_content,
        written_content::list_written_contents,
        written_content::get_written_content_count,
        // MountainPeak
        mountain_peaks::get_mountain_peak,
        mountain_peaks::list_mountain_peaks,
        mountain_peaks::get_mountain_peak_count,
        // HERelationship
        he_relationships::get_he_relationship,
        he_relationships::list_he_relationships,
        he_relationships::get_he_relationship_count,
        // HERelationshipSupplement
        he_relationship_supplements::get_he_relationship_supplement,
        he_relationship_supplements::list_he_relationship_supplements,
        he_relationship_supplements::get_he_relationship_supplement_count,
        // Entity
        entities::get_entity,
        entities::list_entities,
        entities::get_entity_count,
        // HistoricalEventCollection
        historical_event_collections::get_historical_event_collection,
        historical_event_collections::list_historical_event_collections,
        historical_event_collections::get_historical_event_collection_count,
        // Creature
        creatures::get_creature,
        creatures::list_creatures,
        creatures::get_creature_count,
        // DF Storyteller info
        df_st_info::get_df_st_info,
        // MapImage
        map_images::get_map_image,
        map_images::list_map_images,
        map_images::get_map_image_count,
        map_images::get_map_image_png,
        // SiteMapImage
        site_map_images::get_site_map_image,
        site_map_images::list_site_map_images,
        site_map_images::get_site_map_image_count,
        site_map_images::get_site_map_image_png,
    ];
    (spec, routes)
}

/// Return a List of Routes created by Rocket
pub fn get_routes() -> Vec<Route> {
    get_spec_and_routes().1
}

/// Return a OpenApi object that can be turned into json
pub fn get_openapi_spec() -> OpenApi {
    get_spec_and_routes().0
}
