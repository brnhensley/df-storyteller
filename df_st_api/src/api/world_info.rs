use crate::api_errors::{APIErrorNoContent, APIErrorNotModified};
use crate::api_objects::{
    ApiCountPage, ApiItem, ApiMinimalPagination, ApiObject, ApiPage, ApiPagination,
};
use crate::DfStDatabase;
use crate::ServerInfo;
use df_st_core::item_count::ItemCount;
use df_st_db::{id_filter, DBObject};
use rocket::{get, State};
use rocket_contrib::json::Json;
use rocket_okapi_codegen::openapi;

impl ApiObject for df_st_core::DFWorldInfo {
    fn get_type() -> String {
        return "world_info".to_owned();
    }
    fn get_item_link(&self, base_url: &String) -> String {
        format!("{}/worlds_info/{}", base_url, self.id)
    }
    fn get_page_link(base_url: &String) -> String {
        format!("{}/worlds_info", base_url)
    }
    fn get_count_link(base_url: &String) -> String {
        format!("{}/worlds_info/count", base_url)
    }
}

/// Request a `DFWorldInfo` by id.
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/worlds_info/<world_id>")]
pub fn get_world_info(
    conn: DfStDatabase,
    world_id: i32,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiItem<df_st_core::DFWorldInfo>>, APIErrorNoContent> {
    let mut api_page = ApiItem::<df_st_core::DFWorldInfo>::new(&server_info);
    let item_search =
        df_st_db::DFWorldInfo::get_from_db(&*conn, id_filter!["id" => world_id], true)?;
    if let Some(item) = item_search {
        api_page.wrap(item);
        return Ok(Json(api_page));
    }
    Err(APIErrorNoContent::new())
}

/// Request a list of all `DFWorldInfo` in the database.
/// List is not ordered and some id's might be missing!
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/worlds_info?<pagination..>")]
pub fn list_worlds_info(
    conn: DfStDatabase,
    pagination: ApiPagination,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiPage<df_st_core::DFWorldInfo>>, APIErrorNotModified> {
    let mut api_page = ApiPage::<df_st_core::DFWorldInfo>::new(&pagination, &server_info);
    api_page.order_by = df_st_db::DFWorldInfo::match_field_by_opt(api_page.order_by);
    api_page.total_item_count =
        df_st_db::DFWorldInfo::get_count_from_db(&*conn, id_filter![], 0, 1, None)?
            .get(0)
            .unwrap()
            .count;

    let result_list = df_st_db::DFWorldInfo::get_list_from_db(
        &*conn,
        id_filter![],
        api_page.page_start,
        api_page.max_page_size,
        api_page.get_db_order(),
        api_page.order_by.clone(),
        true,
    )?;

    if api_page.wrap(result_list) {
        return Err(APIErrorNotModified::new());
    }
    Ok(Json(api_page))
}

/// Request a counts about `DFWorldInfo` grouped by a field.
///
/// ( Since = "0.1.0" )
#[openapi]
#[get("/worlds_info/count?<group_by>&<pagination..>")]
pub fn get_world_info_count(
    conn: DfStDatabase,
    group_by: Option<String>,
    pagination: ApiMinimalPagination,
    server_info: State<ServerInfo>,
) -> Result<Json<ApiCountPage<ItemCount, df_st_core::DFWorldInfo>>, APIErrorNotModified> {
    let mut api_page =
        ApiCountPage::<ItemCount, df_st_core::DFWorldInfo>::new(&pagination, &server_info);
    api_page.group_by = df_st_db::DFWorldInfo::match_field_by_opt(group_by);
    api_page.total_item_count = df_st_db::DFWorldInfo::get_count_from_db(
        &*conn,
        id_filter![],
        0,
        u32::MAX,
        api_page.group_by.clone(),
    )?
    .len() as u32;

    let result_list = df_st_db::DFWorldInfo::get_count_from_db(
        &*conn,
        id_filter![],
        api_page.page_start,
        api_page.max_page_size,
        api_page.group_by.clone(),
    )?;

    if api_page.wrap(result_list) {
        return Err(APIErrorNotModified::new());
    }
    Ok(Json(api_page))
}
