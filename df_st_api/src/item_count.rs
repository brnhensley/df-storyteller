use crate::api_objects::ApiObject;
use df_st_core::item_count::ItemCount;

impl ApiObject for ItemCount {
    fn get_type() -> String {
        return "item_count".to_owned();
    }
    fn get_item_link(&self, base_url: &String) -> String {
        base_url.clone()
    }
    fn get_page_link(base_url: &String) -> String {
        base_url.clone()
    }
    fn get_count_link(base_url: &String) -> String {
        base_url.clone()
    }
}
