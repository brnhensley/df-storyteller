#![forbid(unsafe_code)]
#![deny(clippy::all)]

mod cli;

/// Start of the DF Storyteller CLI application.
fn main() -> std::io::Result<()> {
    cli::main()
}
