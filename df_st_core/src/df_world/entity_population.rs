use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// The population of an entity, usually civilization in the world.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct EntityPopulation {
    /// Identifier for the entity population object.
    /// `id` must be unique for the whole world.
    pub id: i32,
    pub civ_id: Option<i32>,
    pub races: Vec<String>,
}

impl EntityPopulation {
    pub fn new() -> Self {
        Self::default()
    }
}

impl CreateNew for EntityPopulation {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for EntityPopulation {
    fn example() -> Self {
        Self::default()
    }
}
