use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// An Historical Era is period of time with some defining feature in the world.
/// The are often refereed to as
/// [ages](https://dwarffortresswiki.org/index.php/Ages)
/// The world might go through 1 or multiple ages. The ages change when
/// the amount of powerful creatures or civilizations reaches some threshold.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct HistoricalEra {
    /// Identifier for the era.
    /// `id` must be unique for the whole world.
    /// This value is not given from files but generated after parsing.
    pub id: i32,
    /// The name of the historical era.
    /// The names follow a specific criteria that can be found
    /// [here](https://dwarffortresswiki.org/index.php/Ages).
    /// It most often start with "The Age of ...".
    pub name: Option<String>,
    /// The year the historical era started with.
    /// The first are starts in the year "-1"
    pub start_year: Option<i32>, // TODO Date
}

impl HistoricalEra {
    pub fn new() -> Self {
        Self::default()
    }
}

impl CreateNew for HistoricalEra {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for HistoricalEra {
    fn example() -> Self {
        Self::default()
    }
}
