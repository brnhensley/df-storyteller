use crate::create_new::CreateNew;
use crate::fillable::{Fillable, Filler};
use crate::positions::{Coordinate, Path};
use crate::SchemaExample;
use df_st_derive::{Fillable, Filler, HashAndPartialEqById};
use juniper::GraphQLObject;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

/// An River is a stream of water in the world.
#[derive(
    Serialize,
    Deserialize,
    Clone,
    Debug,
    HashAndPartialEqById,
    Fillable,
    Filler,
    Default,
    JsonSchema,
    GraphQLObject,
)]
pub struct River {
    /// Identifier for the river.
    /// `id` must be unique for the whole world.
    /// This value is not given from files but generated after parsing.
    pub id: i32,
    /// The name of the river
    pub name: Option<String>,
    /// The path the stream takes
    /// TODO path is upstream of downstream?
    pub path: Vec<Path>,
    /// The coordinates of the point where the river ends.
    /// This is usually where it meets the see or ocean or another river.
    pub end_pos: Option<Coordinate>,
}

impl River {
    pub fn new() -> Self {
        Self::default()
    }
}

impl CreateNew for River {
    fn new_by_id(id: i32) -> Self {
        Self {
            id,
            ..Default::default()
        }
    }
}

impl SchemaExample for River {
    fn example() -> Self {
        Self::default()
    }
}
