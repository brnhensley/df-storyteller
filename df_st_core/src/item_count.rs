use crate::SchemaExample;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};

#[derive(Serialize, Deserialize, Clone, Debug, Default, JsonSchema)]
#[schemars(example = "Self::example")]
pub struct ItemCount {
    /// The amount of items with this `value`.
    pub count: u32,
    /// The value of the field that it is grouped by.
    /// The value is `total` when no group by is given.
    /// The type varies depending on the group_by field type.
    pub value: Value,
}

impl ItemCount {
    pub fn new() -> Self {
        Self::default()
    }
}

impl SchemaExample for ItemCount {
    fn example() -> Self {
        Self {
            count: 60,
            value: json!("Tundra"),
        }
    }
}
