/// Trait for adds examples to API documentation
pub trait SchemaExample {
    fn example() -> Self;
}
