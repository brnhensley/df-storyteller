
CREATE TABLE artifacts (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  name VARCHAR,
  site_id INTEGER,
  structure_local_id INTEGER,
  holder_hfid INTEGER,

  abs_tile_x INTEGER,
  abs_tile_y INTEGER,
  abs_tile_z INTEGER,
  subregion_id INTEGER,

  item_name VARCHAR,
  item_type VARCHAR,
  item_subtype VARCHAR,
  item_writing INTEGER,
  item_page_number INTEGER,
  item_page_written_content_id INTEGER,
  item_writing_written_content_id INTEGER,
  item_description VARCHAR,
  item_mat VARCHAR,

  PRIMARY KEY (id, world_id),
  FOREIGN KEY (site_id, world_id) REFERENCES sites (id, world_id),
  FOREIGN KEY (site_id, structure_local_id, world_id) 
    REFERENCES sites_structures (site_id, local_id, world_id)
  -- FOREIGN KEY (holder_hfid) REFERENCES ... (id),
  -- FOREIGN KEY (subregion_id) REFERENCES ...? (id),
  -- FOREIGN KEY (item_page_written_content_id) REFERENCES ... (id)
);
