
CREATE TABLE written_contents (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  title VARCHAR,
  author_hf_id INTEGER,
  author_roll INTEGER,
  form VARCHAR,
  form_id INTEGER,
  page_start INTEGER,
  page_end INTEGER,
  type VARCHAR,
  author INTEGER,

  PRIMARY KEY (id, world_id)
);

CREATE TABLE wc_style (
  written_content_id INTEGER NOT NULL,
  local_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,
  
  label VARCHAR,
  weight INTEGER,

  PRIMARY KEY (written_content_id, local_id, world_id),
  FOREIGN KEY (written_content_id, world_id) 
    REFERENCES written_contents (id, world_id)
);

CREATE TABLE wc_reference (
  written_content_parent_id INTEGER NOT NULL,
  local_id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  type VARCHAR,
  type_id INTEGER,

  written_content_id INTEGER,
  he_id INTEGER,
  site_id INTEGER,
  poetic_form_id INTEGER,
  musical_form_id INTEGER,
  dance_form_id INTEGER,
  hf_id INTEGER,
  entity_id INTEGER,
  artifact_id INTEGER,
  subregion_id INTEGER,

  PRIMARY KEY (written_content_parent_id, local_id, world_id),
  FOREIGN KEY (written_content_parent_id, world_id) 
    REFERENCES written_contents (id, world_id)
);