
CREATE TABLE he_relationships (
  id INTEGER NOT NULL,
  world_id INTEGER NOT NULL,

  event INTEGER,
  relationship VARCHAR,
  source_hf_id INTEGER,
  target_hf_id INTEGER,
  year INTEGER,
  
  PRIMARY KEY (id, world_id)
);