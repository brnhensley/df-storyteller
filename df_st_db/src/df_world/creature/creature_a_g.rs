use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{Creature, DBDFWorld};
use crate::schema::creatures_a_g;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "creatures_a_g"]
#[primary_key(cr_id)]
#[belongs_to(Creature, foreign_key = "cr_id")]
pub struct CreatureAG {
    pub cr_id: i32,
    pub world_id: i32,

    pub all_castes_alive: Option<bool>,
    pub artificial_hiveable: Option<bool>,
    pub does_not_exist: Option<bool>,
    pub equipment: Option<bool>,
    pub equipment_wagon: Option<bool>,
    pub evil: Option<bool>,
    pub fanciful: Option<bool>,
    pub generated: Option<bool>,
    pub good: Option<bool>,
}

impl CreatureAG {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::Creature, CreatureAG> for CreatureAG {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, creatures_a_g: &[CreatureAG]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(creatures_a_g::table)
            .values(creatures_a_g)
            .on_conflict((creatures_a_g::cr_id, creatures_a_g::world_id))
            .do_update()
            .set((
                creatures_a_g::all_castes_alive.eq(excluded(creatures_a_g::all_castes_alive)),
                creatures_a_g::artificial_hiveable.eq(excluded(creatures_a_g::artificial_hiveable)),
                creatures_a_g::does_not_exist.eq(excluded(creatures_a_g::does_not_exist)),
                creatures_a_g::equipment.eq(excluded(creatures_a_g::equipment)),
                creatures_a_g::equipment_wagon.eq(excluded(creatures_a_g::equipment_wagon)),
                creatures_a_g::evil.eq(excluded(creatures_a_g::evil)),
                creatures_a_g::fanciful.eq(excluded(creatures_a_g::fanciful)),
                creatures_a_g::generated.eq(excluded(creatures_a_g::generated)),
                creatures_a_g::good.eq(excluded(creatures_a_g::good)),
            ))
            .execute(conn)
            .expect("Error saving creatures_a_g");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, creatures_a_g: &[CreatureAG]) {
        diesel::insert_into(creatures_a_g::table)
            .values(creatures_a_g)
            .execute(conn)
            .expect("Error saving creatures_a_g");
    }

    /// Get a list of CreatureAG from the database
    fn find_db_list(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: i64,
        _limit: i64,
        _order: Option<OrderTypes>,
        _order_by: Option<String>,
    ) -> Result<Vec<CreatureAG>, Error> {
        /*
        use crate::schema::creatures_a_g::dsl::*;
        let (order_by,asc) = Self::get_order(order, order_by);
        let query = creatures_a_g.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "cr_id" => cr_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "cr_id" => cr_id,
                "all_castes_alive" => all_castes_alive,
                "artificial_hiveable" => artificial_hiveable,
                "does_not_exist" => does_not_exist,
                "equipment" => equipment,
                "equipment_wagon" => equipment_wagon,
                "evil" => evil,
                "fanciful" => fanciful,
                "generated" => generated,
                "good" => good,
            });},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<CreatureAG>, Error> {
        use crate::schema::creatures_a_g::dsl::*;
        let query = creatures_a_g;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(cr_id.eq(id_filter.get("cr_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "cr_id" => cr_id,
            ],
            {return Ok(query.first::<CreatureAG>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "all_castes_alive" => "all_castes_alive",
            "artificial_hiveable" => "artificial_hiveable",
            "does_not_exist" => "does_not_exist",
            "equipment" => "equipment",
            "equipment_wagon" => "equipment_wagon",
            "evil" => "evil",
            "fanciful" => "fanciful",
            "generated" => "generated",
            "good" => "good",
            "cr_id" | _ => "cr_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[CreatureAG],
        core_list: Vec<df_st_core::Creature>,
    ) -> Result<Vec<df_st_core::Creature>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        _conn: &DbConnection,
        _id_filter: HashMap<String, i32>,
        _offset: u32,
        _limit: u32,
        _group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        /*
        use crate::schema::creatures_a_g::dsl::*;
        let query = creatures_a_g.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter!{
            query, id_filter,
            [
                "cr_id" => cr_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "cr_id" => {cr_id: i32},
                "all_castes_alive" => {all_castes_alive: Option<bool>},
                "artificial_hiveable" => {artificial_hiveable: Option<bool>},
                "does_not_exist" => {does_not_exist: Option<bool>},
                "equipment" => {equipment: Option<bool>},
                "equipment_wagon" => {equipment_wagon: Option<bool>},
                "evil" => {evil: Option<bool>},
                "fanciful" => {fanciful: Option<bool>},
                "generated" => {generated: Option<bool>},
                "good" => {good: Option<bool>},
            };},
        };
        */
        // This function is not used ATM, replaced with simple result to reduce compile time.
        Ok(vec![])
    }
}

/// From Core to DB
impl Filler<CreatureAG, df_st_core::Creature> for CreatureAG {
    fn add_missing_data(&mut self, source: &df_st_core::Creature) {
        self.cr_id.add_missing_data(&source.id);
        self.all_castes_alive
            .add_missing_data(&source.all_castes_alive);
        self.artificial_hiveable
            .add_missing_data(&source.artificial_hiveable);
        self.does_not_exist.add_missing_data(&source.does_not_exist);
        self.equipment.add_missing_data(&source.equipment);
        self.equipment_wagon
            .add_missing_data(&source.equipment_wagon);
        self.evil.add_missing_data(&source.evil);
        self.fanciful.add_missing_data(&source.fanciful);
        self.generated.add_missing_data(&source.generated);
        self.good.add_missing_data(&source.good);
    }
}

/// From DB to Core
impl Filler<df_st_core::Creature, CreatureAG> for df_st_core::Creature {
    fn add_missing_data(&mut self, source: &CreatureAG) {
        self.id.add_missing_data(&source.cr_id);
        self.all_castes_alive
            .add_missing_data(&source.all_castes_alive);
        self.artificial_hiveable
            .add_missing_data(&source.artificial_hiveable);
        self.does_not_exist.add_missing_data(&source.does_not_exist);
        self.equipment.add_missing_data(&source.equipment);
        self.equipment_wagon
            .add_missing_data(&source.equipment_wagon);
        self.evil.add_missing_data(&source.evil);
        self.fanciful.add_missing_data(&source.fanciful);
        self.generated.add_missing_data(&source.generated);
        self.good.add_missing_data(&source.good);
    }
}

impl PartialEq for CreatureAG {
    fn eq(&self, other: &Self) -> bool {
        self.cr_id == other.cr_id
    }
}

impl Hash for CreatureAG {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.cr_id.hash(state);
    }
}

impl PartialEq<CreatureAG> for df_st_core::Creature {
    fn eq(&self, other: &CreatureAG) -> bool {
        self.id == other.cr_id
    }
}

impl PartialEq<df_st_core::Creature> for CreatureAG {
    fn eq(&self, other: &df_st_core::Creature) -> bool {
        self.cr_id == other.id
    }
}
