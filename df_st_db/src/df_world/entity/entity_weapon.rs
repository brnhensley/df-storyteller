use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, Entity};
use crate::schema::entity_weapons;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "entity_weapons"]
#[primary_key(en_id, weapon)]
#[belongs_to(Entity, foreign_key = "en_id")]
pub struct EntityWeapon {
    pub en_id: i32,
    pub weapon: String,
    pub world_id: i32,
}

impl EntityWeapon {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<EntityWeapon, EntityWeapon> for EntityWeapon {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, entity_weapons: &[EntityWeapon]) {
        diesel::insert_into(entity_weapons::table)
            .values(entity_weapons)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving entity_weapons");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, entity_weapons: &[EntityWeapon]) {
        diesel::insert_into(entity_weapons::table)
            .values(entity_weapons)
            .execute(conn)
            .expect("Error saving entity_weapons");
    }

    /// Get a list of EntityWeapon from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<EntityWeapon>, Error> {
        use crate::schema::entity_weapons::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = entity_weapons.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "en_id" => en_id,
                "weapon" => weapon,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<EntityWeapon>, Error> {
        use crate::schema::entity_weapons::dsl::*;
        let query = entity_weapons;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(en_id.eq(id_filter.get("en_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
            ],
            {return Ok(query.first::<EntityWeapon>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "weapon" => "weapon",
            "en_id" | _ => "en_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[EntityWeapon],
        core_list: Vec<EntityWeapon>,
    ) -> Result<Vec<EntityWeapon>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::entity_weapons::dsl::*;
        let query = entity_weapons.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "en_id" => en_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "en_id" => {en_id: i32},
                "weapon" => {weapon: String},
            };},
        };
    }
}

impl PartialEq for EntityWeapon {
    fn eq(&self, other: &Self) -> bool {
        self.en_id == other.en_id && self.weapon == other.weapon
    }
}

impl Hash for EntityWeapon {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.en_id.hash(state);
        self.weapon.hash(state);
    }
}
