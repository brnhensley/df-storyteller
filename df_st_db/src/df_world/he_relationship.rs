use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::he_relationships;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use failure::Error;
use std::collections::HashMap;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "he_relationships"]
pub struct HERelationship {
    pub id: i32,
    pub world_id: i32,
    pub event: Option<i32>,
    pub relationship: Option<String>,
    pub source_hf_id: Option<i32>,
    pub target_hf_id: Option<i32>,
    pub year: Option<i32>,
}

impl HERelationship {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HERelationship, HERelationship> for HERelationship {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, he_relationships: &[HERelationship]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(he_relationships::table)
            .values(he_relationships)
            .on_conflict((he_relationships::id, he_relationships::world_id))
            .do_update()
            .set((
                he_relationships::event.eq(excluded(he_relationships::event)),
                he_relationships::relationship.eq(excluded(he_relationships::relationship)),
                he_relationships::source_hf_id.eq(excluded(he_relationships::source_hf_id)),
                he_relationships::target_hf_id.eq(excluded(he_relationships::target_hf_id)),
                he_relationships::year.eq(excluded(he_relationships::year)),
            ))
            .execute(conn)
            .expect("Error saving he_relationships");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, he_relationships: &[HERelationship]) {
        diesel::insert_into(he_relationships::table)
            .values(he_relationships)
            .execute(conn)
            .expect("Error saving he_relationships");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HERelationship>, Error> {
        use crate::schema::he_relationships::dsl::*;
        let query = he_relationships;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        return Ok(query.first::<HERelationship>(conn).optional()?);
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<HERelationship>, Error> {
        use crate::schema::he_relationships::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = he_relationships.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "event" => event,
                "relationship" => relationship,
                "source_hf_id" => source_hf_id,
                "target_hf_id" => target_hf_id,
                "year" => year,
            });},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "event" => "event",
            "relationship" => "relationship",
            "source_hf_id" => "source_hf_id",
            "target_hf_id" => "target_hf_id",
            "year" => "year",
            "id" | _ => "id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HERelationship],
        core_list: Vec<df_st_core::HERelationship>,
    ) -> Result<Vec<df_st_core::HERelationship>, Error> {
        // Nothing to add
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::he_relationships::dsl::*;
        let query = he_relationships.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "event" => {event: Option<i32>},
                "relationship" => {relationship: Option<String>},
                "source_hf_id" => {source_hf_id: Option<i32>},
                "target_hf_id" => {target_hf_id: Option<i32>},
                "year" => {year: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HERelationship, df_st_core::HERelationship> for HERelationship {
    fn add_missing_data(&mut self, source: &df_st_core::HERelationship) {
        self.id.add_missing_data(&source.id);
        self.event.add_missing_data(&source.event);
        self.relationship.add_missing_data(&source.relationship);
        self.source_hf_id.add_missing_data(&source.source_hf_id);
        self.target_hf_id.add_missing_data(&source.target_hf_id);
        self.year.add_missing_data(&source.year);
    }
}

/// From DB to Core
impl Filler<df_st_core::HERelationship, HERelationship> for df_st_core::HERelationship {
    fn add_missing_data(&mut self, source: &HERelationship) {
        self.id.add_missing_data(&source.id);
        self.event.add_missing_data(&source.event);
        self.relationship.add_missing_data(&source.relationship);
        self.source_hf_id.add_missing_data(&source.source_hf_id);
        self.target_hf_id.add_missing_data(&source.target_hf_id);
        self.year.add_missing_data(&source.year);
    }
}

impl PartialEq<HERelationship> for df_st_core::HERelationship {
    fn eq(&self, other: &HERelationship) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::HERelationship> for HERelationship {
    fn eq(&self, other: &df_st_core::HERelationship) -> bool {
        self.id == other.id
    }
}
