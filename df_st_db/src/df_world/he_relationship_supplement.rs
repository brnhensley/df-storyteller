use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::DBDFWorld;
use crate::schema::he_relationship_supplements;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, HashAndPartialEqById};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use failure::Error;
use std::collections::HashMap;

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    HashAndPartialEqById,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "he_relationship_supplements"]
pub struct HERelationshipSupplement {
    pub id: i32,
    pub world_id: i32,
    pub event: Option<i32>,
    pub occasion_type: Option<i32>,
    pub site_id: Option<i32>,
    pub unk_1: Option<i32>,
}

impl HERelationshipSupplement {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HERelationshipSupplement, HERelationshipSupplement>
    for HERelationshipSupplement
{
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(
        conn: &DbConnection,
        he_relationship_supplements: &[HERelationshipSupplement],
    ) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(he_relationship_supplements::table)
            .values(he_relationship_supplements)
            .on_conflict((
                he_relationship_supplements::id,
                he_relationship_supplements::world_id,
            ))
            .do_update()
            .set((
                he_relationship_supplements::event.eq(excluded(he_relationship_supplements::event)),
                he_relationship_supplements::occasion_type
                    .eq(excluded(he_relationship_supplements::occasion_type)),
                he_relationship_supplements::site_id
                    .eq(excluded(he_relationship_supplements::site_id)),
                he_relationship_supplements::unk_1.eq(excluded(he_relationship_supplements::unk_1)),
            ))
            .execute(conn)
            .expect("Error saving he_relationship_supplements");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(
        conn: &DbConnection,
        he_relationship_supplements: &[HERelationshipSupplement],
    ) {
        diesel::insert_into(he_relationship_supplements::table)
            .values(he_relationship_supplements)
            .execute(conn)
            .expect("Error saving he_relationship_supplements");
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HERelationshipSupplement>, Error> {
        use crate::schema::he_relationship_supplements::dsl::*;
        let query = he_relationship_supplements;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(id.eq(id_filter.get("id").unwrap_or(&0)));
        return Ok(query.first::<HERelationshipSupplement>(conn).optional()?);
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<HERelationshipSupplement>, Error> {
        use crate::schema::he_relationship_supplements::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = he_relationship_supplements.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "event" => event,
                "occasion_type" => occasion_type,
                "site_id" => site_id,
                "unk_1" => unk_1,
            });},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "event" => "event",
            "occasion_type" => "occasion_type",
            "site_id" => "site_id",
            "unk_1" => "unk_1",
            "id" | _ => "id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HERelationshipSupplement],
        core_list: Vec<df_st_core::HERelationshipSupplement>,
    ) -> Result<Vec<df_st_core::HERelationshipSupplement>, Error> {
        // Nothing to add
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::he_relationship_supplements::dsl::*;
        let query = he_relationship_supplements
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "event" => {event: Option<i32>},
                "occasion_type" => {occasion_type: Option<i32>},
                "site_id" => {site_id: Option<i32>},
                "unk_1" => {unk_1: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HERelationshipSupplement, df_st_core::HERelationshipSupplement>
    for HERelationshipSupplement
{
    fn add_missing_data(&mut self, source: &df_st_core::HERelationshipSupplement) {
        self.id.add_missing_data(&source.id);
        self.event.add_missing_data(&source.event);
        self.occasion_type.add_missing_data(&source.occasion_type);
        self.site_id.add_missing_data(&source.site_id);
        self.unk_1.add_missing_data(&source.unk_1);
    }
}

/// From DB to Core
impl Filler<df_st_core::HERelationshipSupplement, HERelationshipSupplement>
    for df_st_core::HERelationshipSupplement
{
    fn add_missing_data(&mut self, source: &HERelationshipSupplement) {
        self.id.add_missing_data(&source.id);
        self.event.add_missing_data(&source.event);
        self.occasion_type.add_missing_data(&source.occasion_type);
        self.site_id.add_missing_data(&source.site_id);
        self.unk_1.add_missing_data(&source.unk_1);
    }
}

impl PartialEq<HERelationshipSupplement> for df_st_core::HERelationshipSupplement {
    fn eq(&self, other: &HERelationshipSupplement) -> bool {
        self.id == other.id
    }
}

impl PartialEq<df_st_core::HERelationshipSupplement> for HERelationshipSupplement {
    fn eq(&self, other: &df_st_core::HERelationshipSupplement) -> bool {
        self.id == other.id
    }
}
