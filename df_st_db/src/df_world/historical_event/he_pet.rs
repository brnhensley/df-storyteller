use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEvent};
use crate::schema::he_pets;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "he_pets"]
#[primary_key(he_id, pet)]
#[belongs_to(HistoricalEvent, foreign_key = "he_id")]
pub struct HEPet {
    pub he_id: i32,
    pub pet: String,
    pub world_id: i32,
}

impl HEPet {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<HEPet, HEPet> for HEPet {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, he_pets: &[HEPet]) {
        diesel::insert_into(he_pets::table)
            .values(he_pets)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving he_pets");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, he_pets: &[HEPet]) {
        diesel::insert_into(he_pets::table)
            .values(he_pets)
            .execute(conn)
            .expect("Error saving he_pets");
    }

    /// Get a list of HEPet from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<HEPet>, Error> {
        use crate::schema::he_pets::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = he_pets.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "he_id" => he_id,
                "pet" => pet,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HEPet>, Error> {
        use crate::schema::he_pets::dsl::*;
        let query = he_pets;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(he_id.eq(id_filter.get("he_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {return Ok(query.first::<HEPet>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "pet" => "pet",
            "he_id" | _ => "he_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HEPet],
        core_list: Vec<HEPet>,
    ) -> Result<Vec<HEPet>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::he_pets::dsl::*;
        let query = he_pets.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "he_id" => he_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "he_id" => {he_id: i32},
                "pet" => {pet: String},
            };},
        };
    }
}

impl PartialEq for HEPet {
    fn eq(&self, other: &Self) -> bool {
        self.he_id == other.he_id && self.pet == other.pet
    }
}

impl Hash for HEPet {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.he_id.hash(state);
        self.pet.hash(state);
    }
}
