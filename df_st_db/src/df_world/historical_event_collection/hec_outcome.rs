use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalEventCollection};
use crate::schema::hec_outcomes;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone, Debug, Identifiable, Associations, Filler, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "hec_outcomes"]
#[primary_key(hec_id, outcome)]
#[belongs_to(HistoricalEventCollection, foreign_key = "hec_id")]
pub struct HECOutcome {
    pub hec_id: i32,
    pub outcome: String,
    pub world_id: i32,
}

impl HECOutcome {
    pub fn new() -> Self {
        Self::default()
    }
}

// There is no core variant of this item, so implement it for itself.
impl DBObject<HECOutcome, HECOutcome> for HECOutcome {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hec_outcomes: &[HECOutcome]) {
        diesel::insert_into(hec_outcomes::table)
            .values(hec_outcomes)
            .on_conflict_do_nothing()
            .execute(conn)
            .expect("Error saving hec_outcomes");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hec_outcomes: &[HECOutcome]) {
        diesel::insert_into(hec_outcomes::table)
            .values(hec_outcomes)
            .execute(conn)
            .expect("Error saving hec_outcomes");
    }

    /// Get a list of HECOutcome from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<HECOutcome>, Error> {
        use crate::schema::hec_outcomes::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hec_outcomes.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "hec_id" => hec_id,
                "outcome" => outcome,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HECOutcome>, Error> {
        use crate::schema::hec_outcomes::dsl::*;
        let query = hec_outcomes;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hec_id.eq(id_filter.get("hec_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {return Ok(query.first::<HECOutcome>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "outcome" => "outcome",
            "hec_id" | _ => "hec_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HECOutcome],
        core_list: Vec<HECOutcome>,
    ) -> Result<Vec<HECOutcome>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hec_outcomes::dsl::*;
        let query = hec_outcomes.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hec_id" => hec_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hec_id" => {hec_id: i32},
                "outcome" => {outcome: String},
            };},
        };
    }
}

impl PartialEq for HECOutcome {
    fn eq(&self, other: &Self) -> bool {
        self.hec_id == other.hec_id && self.outcome == other.outcome
    }
}

impl Hash for HECOutcome {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hec_id.hash(state);
        self.outcome.hash(state);
    }
}
