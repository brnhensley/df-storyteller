use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_entity_position_links;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    Associations,
    Filler,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "hf_entity_position_links"]
#[primary_key(id)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFEntityPositionLink {
    pub id: i32,
    pub world_id: i32,

    pub hf_id: i32,
    pub entity_id: i32,

    pub position_profile_id: Option<i32>,
    pub start_year: Option<i32>,
    pub end_year: Option<i32>,
}

impl HFEntityPositionLink {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HFEntityPositionLink, HFEntityPositionLink> for HFEntityPositionLink {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hf_entity_position_links: &[HFEntityPositionLink]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(hf_entity_position_links::table)
            .values(hf_entity_position_links)
            .on_conflict((
                hf_entity_position_links::id,
                hf_entity_position_links::world_id,
            ))
            .do_update()
            .set((
                hf_entity_position_links::hf_id.eq(excluded(hf_entity_position_links::hf_id)),
                hf_entity_position_links::entity_id
                    .eq(excluded(hf_entity_position_links::entity_id)),
                hf_entity_position_links::position_profile_id
                    .eq(excluded(hf_entity_position_links::position_profile_id)),
                hf_entity_position_links::start_year
                    .eq(excluded(hf_entity_position_links::start_year)),
                hf_entity_position_links::end_year.eq(excluded(hf_entity_position_links::end_year)),
            ))
            .execute(conn)
            .expect("Error saving hf_entity_position_links");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hf_entity_position_links: &[HFEntityPositionLink]) {
        diesel::insert_into(hf_entity_position_links::table)
            .values(hf_entity_position_links)
            .execute(conn)
            .expect("Error saving hf_entity_position_links");
    }

    /// Get a list of HFEntityPositionLink from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<HFEntityPositionLink>, Error> {
        use crate::schema::hf_entity_position_links::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_entity_position_links.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
                "hf_id" => hf_id,
                "entity_id" => entity_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "id" => id,
                "hf_id" => hf_id,
                "entity_id" => entity_id,
                "position_profile_id" => position_profile_id,
                "start_year" => start_year,
                "end_year" => end_year,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFEntityPositionLink>, Error> {
        use crate::schema::hf_entity_position_links::dsl::*;
        let query = hf_entity_position_links;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
                "hf_id" => hf_id,
                "entity_id" => entity_id,
            ],
            {return Ok(query.first::<HFEntityPositionLink>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "entity_id" => "entity_id",
            "hf_id" => "hf_id",
            "id" | _ => "id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFEntityPositionLink],
        core_list: Vec<df_st_core::HFEntityPositionLink>,
    ) -> Result<Vec<df_st_core::HFEntityPositionLink>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_entity_position_links::dsl::*;
        let query = hf_entity_position_links
            .limit(limit as i64)
            .offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "id" => id,
                "hf_id" => hf_id,
                "entity_id" => entity_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "id" => {id: i32},
                "hf_id" => {hf_id: i32},
                "entity_id" => {entity_id: i32},
                "position_profile_id" => {position_profile_id: Option<i32>},
                "start_year" => {start_year: Option<i32>},
                "end_year" => {end_year: Option<i32>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HFEntityPositionLink, df_st_core::HFEntityPositionLink> for HFEntityPositionLink {
    fn add_missing_data(&mut self, source: &df_st_core::HFEntityPositionLink) {
        self.id.add_missing_data(&source.id);
        self.hf_id.add_missing_data(&source.hf_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.position_profile_id
            .add_missing_data(&source.position_profile_id);
        self.start_year.add_missing_data(&source.start_year);
        self.end_year.add_missing_data(&source.end_year);
    }
}

/// From DB to Core
impl Filler<df_st_core::HFEntityPositionLink, HFEntityPositionLink>
    for df_st_core::HFEntityPositionLink
{
    fn add_missing_data(&mut self, source: &HFEntityPositionLink) {
        self.id.add_missing_data(&source.id);
        self.hf_id.add_missing_data(&source.hf_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.position_profile_id
            .add_missing_data(&source.position_profile_id);
        self.start_year.add_missing_data(&source.start_year);
        self.end_year.add_missing_data(&source.end_year);
    }
}

impl PartialEq<df_st_core::HFEntityPositionLink> for HFEntityPositionLink {
    fn eq(&self, other: &df_st_core::HFEntityPositionLink) -> bool {
        self.id == other.id
    }
}

impl PartialEq<HFEntityPositionLink> for df_st_core::HFEntityPositionLink {
    fn eq(&self, other: &HFEntityPositionLink) -> bool {
        self.id == other.id
    }
}

impl PartialEq for HFEntityPositionLink {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Hash for HFEntityPositionLink {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}
