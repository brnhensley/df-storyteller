use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, HistoricalFigure};
use crate::schema::hf_intrigue_actors;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::{Fillable, Filler};
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use failure::Error;
use std::collections::HashMap;
use std::hash::{Hash, Hasher};

#[derive(
    Clone,
    Debug,
    AsChangeset,
    Identifiable,
    Associations,
    Filler,
    Queryable,
    Insertable,
    Fillable,
    Default,
)]
#[table_name = "hf_intrigue_actors"]
#[primary_key(hf_id, local_id)]
#[belongs_to(HistoricalFigure, foreign_key = "hf_id")]
pub struct HFIntrigueActor {
    pub hf_id: i32,
    pub local_id: i32,
    pub world_id: i32,
    pub entity_id: Option<i32>,
    pub hf_id_other: Option<i32>,
    pub role: Option<String>,
    pub strategy: Option<String>,
    pub strategy_en_id: Option<i32>,
    pub strategy_epp_id: Option<i32>,
    pub handle_actor_id: Option<i32>,
    pub promised_actor_immortality: Option<bool>,
    pub promised_me_immortality: Option<bool>,
}

impl HFIntrigueActor {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::HFIntrigueActor, HFIntrigueActor> for HFIntrigueActor {
    fn add_missing_data_advanced(_core_world: &df_st_core::DFWorld, _world: &mut DBDFWorld) {
        // Nothing to add
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, hf_intrigue_actors: &[HFIntrigueActor]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(hf_intrigue_actors::table)
            .values(hf_intrigue_actors)
            .on_conflict((
                hf_intrigue_actors::hf_id,
                hf_intrigue_actors::local_id,
                hf_intrigue_actors::world_id,
            ))
            .do_update()
            .set((
                hf_intrigue_actors::entity_id.eq(excluded(hf_intrigue_actors::entity_id)),
                hf_intrigue_actors::hf_id_other.eq(excluded(hf_intrigue_actors::hf_id_other)),
                hf_intrigue_actors::role.eq(excluded(hf_intrigue_actors::role)),
                hf_intrigue_actors::strategy.eq(excluded(hf_intrigue_actors::strategy)),
                hf_intrigue_actors::strategy_en_id.eq(excluded(hf_intrigue_actors::strategy_en_id)),
                hf_intrigue_actors::strategy_epp_id
                    .eq(excluded(hf_intrigue_actors::strategy_epp_id)),
                hf_intrigue_actors::handle_actor_id
                    .eq(excluded(hf_intrigue_actors::handle_actor_id)),
                hf_intrigue_actors::promised_actor_immortality
                    .eq(excluded(hf_intrigue_actors::promised_actor_immortality)),
                hf_intrigue_actors::promised_me_immortality
                    .eq(excluded(hf_intrigue_actors::promised_me_immortality)),
            ))
            .execute(conn)
            .expect("Error saving hf_intrigue_actors");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, hf_intrigue_actors: &[HFIntrigueActor]) {
        diesel::insert_into(hf_intrigue_actors::table)
            .values(hf_intrigue_actors)
            .execute(conn)
            .expect("Error saving hf_intrigue_actors");
    }

    /// Get a list of HFIntrigueActor from the database
    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<HFIntrigueActor>, Error> {
        use crate::schema::hf_intrigue_actors::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = hf_intrigue_actors.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "local_id" => local_id,
            ],
            {return Ok(order_by!{
                order_by, asc, query, conn,
                "hf_id" => hf_id,
                "local_id" => local_id,
                "entity_id" => entity_id,
                "hf_id_other" => hf_id_other,
                "role" => role,
                "strategy" => strategy,
                "strategy_en_id" => strategy_en_id,
                "strategy_epp_id" => strategy_epp_id,
                "handle_actor_id" => handle_actor_id,
                "promised_actor_immortality" => promised_actor_immortality,
                "promised_me_immortality" => promised_me_immortality,
            });},
        };
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<HFIntrigueActor>, Error> {
        use crate::schema::hf_intrigue_actors::dsl::*;
        let query = hf_intrigue_actors;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(hf_id.eq(id_filter.get("hf_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "local_id" => local_id,
            ],
            {return Ok(query.first::<HFIntrigueActor>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "local_id" => "local_id",
            "entity_id" => "entity_id",
            "hf_id_other" => "hf_id_other",
            "role" => "role",
            "strategy" => "strategy",
            "strategy_en_id" => "strategy_en_id",
            "strategy_epp_id" => "strategy_epp_id",
            "handle_actor_id" => "handle_actor_id",
            "promised_actor_immortality" => "promised_actor_immortality",
            "promised_me_immortality" => "promised_me_immortality",
            "hf_id" | _ => "hf_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[HFIntrigueActor],
        core_list: Vec<df_st_core::HFIntrigueActor>,
    ) -> Result<Vec<df_st_core::HFIntrigueActor>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::hf_intrigue_actors::dsl::*;
        let query = hf_intrigue_actors.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "hf_id" => hf_id,
                "local_id" => local_id,
            ],
            {group_by!{
                group_by_opt, query, conn,
                "hf_id" => {hf_id: i32},
                "local_id" => {local_id: i32},
                "entity_id" => {entity_id: Option<i32>},
                "hf_id_other" => {hf_id_other: Option<i32>},
                "role" => {role: Option<String>},
                "strategy" => {strategy: Option<String>},
                "strategy_en_id" => {strategy_en_id: Option<i32>},
                "strategy_epp_id" => {strategy_epp_id: Option<i32>},
                "handle_actor_id" => {handle_actor_id: Option<i32>},
                "promised_actor_immortality" => {promised_actor_immortality: Option<bool>},
                "promised_me_immortality" => {promised_me_immortality: Option<bool>},
            };},
        };
    }
}

/// From Core to DB
impl Filler<HFIntrigueActor, df_st_core::HFIntrigueActor> for HFIntrigueActor {
    fn add_missing_data(&mut self, source: &df_st_core::HFIntrigueActor) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.local_id.add_missing_data(&source.local_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.hf_id_other.add_missing_data(&source.hf_id_other);
        self.role.add_missing_data(&source.role);
        self.strategy.add_missing_data(&source.strategy);
        self.strategy_en_id.add_missing_data(&source.strategy_en_id);
        self.strategy_epp_id
            .add_missing_data(&source.strategy_epp_id);
        self.handle_actor_id
            .add_missing_data(&source.handle_actor_id);
        self.promised_actor_immortality
            .add_missing_data(&source.promised_actor_immortality);
        self.promised_me_immortality
            .add_missing_data(&source.promised_me_immortality);
    }
}

/// From DB to Core
impl Filler<df_st_core::HFIntrigueActor, HFIntrigueActor> for df_st_core::HFIntrigueActor {
    fn add_missing_data(&mut self, source: &HFIntrigueActor) {
        self.hf_id.add_missing_data(&source.hf_id);
        self.local_id.add_missing_data(&source.local_id);
        self.entity_id.add_missing_data(&source.entity_id);
        self.hf_id_other.add_missing_data(&source.hf_id_other);
        self.role.add_missing_data(&source.role);
        self.strategy.add_missing_data(&source.strategy);
        self.strategy_en_id.add_missing_data(&source.strategy_en_id);
        self.strategy_epp_id
            .add_missing_data(&source.strategy_epp_id);
        self.handle_actor_id
            .add_missing_data(&source.handle_actor_id);
        self.promised_actor_immortality
            .add_missing_data(&source.promised_actor_immortality);
        self.promised_me_immortality
            .add_missing_data(&source.promised_me_immortality);
    }
}

impl PartialEq<df_st_core::HFIntrigueActor> for HFIntrigueActor {
    fn eq(&self, other: &df_st_core::HFIntrigueActor) -> bool {
        self.hf_id == other.hf_id && self.local_id == other.local_id
    }
}

impl PartialEq<HFIntrigueActor> for df_st_core::HFIntrigueActor {
    fn eq(&self, other: &HFIntrigueActor) -> bool {
        self.hf_id == other.hf_id && self.local_id == other.local_id
    }
}

impl PartialEq for HFIntrigueActor {
    fn eq(&self, other: &Self) -> bool {
        self.hf_id == other.hf_id && self.local_id == other.local_id
    }
}

impl Hash for HFIntrigueActor {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.hf_id.hash(state);
        self.local_id.hash(state);
    }
}
