use crate::db_object::{DBObject, OrderTypes};
use crate::df_world::{DBDFWorld, Site};
use crate::schema::sites_structures;
use crate::DbConnection;
use df_st_core::fillable::{Fillable, Filler};
use df_st_core::item_count::ItemCount;
use df_st_derive::Fillable;
use diesel::expression_methods::ExpressionMethods;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::Queryable;
use failure::Error;
use std::collections::HashMap;

#[derive(
    Clone, Debug, AsChangeset, Identifiable, Associations, Queryable, Insertable, Fillable, Default,
)]
#[table_name = "sites_structures"]
#[primary_key(site_id, local_id)]
#[belongs_to(Site)]
// #[belongs_to(Coordinate)]
pub struct Structure {
    pub site_id: i32,
    pub local_id: i32,
    pub world_id: i32,
    pub type_: Option<String>,
    pub subtype: Option<String>,
    pub name: Option<String>,
    pub name2: Option<String>,
    pub entity_id: Option<i32>,
    pub worship_hfid: Option<i32>,
    // temple
    pub deity_type: Option<i32>,
    pub deity_id: Option<i32>,
    pub religion_id: Option<i32>,
    // dungeon
    pub dungeon_type: Option<i32>,
}

impl Structure {
    pub fn new() -> Self {
        Self::default()
    }
}

impl DBObject<df_st_core::Structure, Structure> for Structure {
    fn add_missing_data_advanced(core_world: &df_st_core::DFWorld, world: &mut DBDFWorld) {
        for site in core_world.sites.values() {
            for structure in &site.structures {
                let mut db_structure = Structure::new();
                db_structure.add_missing_data(&structure);
                db_structure.site_id = site.id;
                world.structures.push(db_structure);
            }
        }
    }

    #[cfg(feature = "postgres")]
    fn insert_into_db(conn: &DbConnection, sites_structures: &[Structure]) {
        use diesel::pg::upsert::excluded;
        diesel::insert_into(sites_structures::table)
            .values(sites_structures)
            .on_conflict((
                sites_structures::site_id,
                sites_structures::local_id,
                sites_structures::world_id,
            ))
            .do_update()
            .set((
                sites_structures::site_id.eq(excluded(sites_structures::site_id)),
                sites_structures::local_id.eq(excluded(sites_structures::local_id)),
            ))
            .execute(conn)
            .expect("Error saving Structure");
    }

    #[cfg(not(feature = "postgres"))]
    fn insert_into_db(conn: &DbConnection, sites_structures: &[Structure]) {
        diesel::insert_into(sites_structures::table)
            .values(sites_structures)
            .execute(conn)
            .expect("Error saving Structure");
    }

    fn find_db_list(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: i64,
        limit: i64,
        order: Option<OrderTypes>,
        order_by: Option<String>,
    ) -> Result<Vec<Structure>, Error> {
        use crate::schema::sites_structures::dsl::*;
        let (order_by, asc) = Self::get_order(order, order_by);
        let query = sites_structures.limit(limit).offset(offset);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        if id_filter.get("site_id").is_some() {
            let query = query.filter(site_id.eq(id_filter.get("site_id").unwrap_or(&0)));
            optional_filter! {
                query, id_filter,
                [
                    "local_id" => local_id,
                ],
                {return Ok(order_by!{
                    order_by, asc, query, conn,
                    "site_id" => site_id,
                    "local_id" => local_id,
                    "type" => type_,
                    "name" => name,
                    "name2" => name2,
                    "entity_id" => entity_id,
                    "worship_hfid" => worship_hfid,
                    "deity_type" => deity_type,
                    "deity_id" => deity_id,
                    "religion_id" => religion_id,
                    "dungeon_type" => dungeon_type,
                });},
            };
        } else {
            optional_filter! {
                query, id_filter,
                [
                    "local_id" => local_id,
                ],
                {return Ok(order_by!{
                    order_by, asc, query, conn,
                    "site_id" => site_id,
                    "local_id" => local_id,
                    "type" => type_,
                    "name" => name,
                    "name2" => name2,
                    "entity_id" => entity_id,
                    "worship_hfid" => worship_hfid,
                    "deity_type" => deity_type,
                    "deity_id" => deity_id,
                    "religion_id" => religion_id,
                    "dungeon_type" => dungeon_type,
                });},
            };
        }
    }

    fn find_db_item(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
    ) -> Result<Option<Structure>, Error> {
        use crate::schema::sites_structures::dsl::*;
        let query = sites_structures;
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        let query = query.filter(site_id.eq(id_filter.get("site_id").unwrap_or(&0)));
        optional_filter! {
            query, id_filter,
            [
                "local_id" => local_id,
            ],
            {return Ok(query.first::<Structure>(conn).optional()?);},
        };
    }

    fn match_field_by(field: String) -> String {
        match field.as_ref() {
            "local_id" => "local_id",
            "type" => "type",
            "subtype" => "subtype",
            "name" => "name",
            "name2" => "name2",
            "entity_id" => "entity_id",
            "worship_hfid" => "worship_hfid",
            "deity_type" => "deity_type",
            "deity_id" => "deity_id",
            "religion_id" => "religion_id",
            "dungeon_type" => "dungeon_type",
            "site_id" | _ => "site_id",
        }
        .to_owned()
    }

    fn add_nested_items(
        _conn: &DbConnection,
        _db_list: &[Structure],
        core_list: Vec<df_st_core::Structure>,
    ) -> Result<Vec<df_st_core::Structure>, Error> {
        Ok(core_list)
    }

    fn get_count_from_db(
        conn: &DbConnection,
        id_filter: HashMap<String, i32>,
        offset: u32,
        limit: u32,
        group_by_opt: Option<String>,
    ) -> Result<Vec<ItemCount>, Error> {
        use crate::schema::sites_structures::dsl::*;
        let query = sites_structures.limit(limit as i64).offset(offset as i64);
        let query = query.filter(world_id.eq(id_filter.get("world_id").unwrap_or(&0)));
        if id_filter.get("site_id").is_some() {
            let query = query.filter(site_id.eq(id_filter.get("site_id").unwrap_or(&0)));
            optional_filter! {
                query, id_filter,
                [
                    "local_id" => local_id,
                ],
                {group_by!{
                    group_by_opt, query, conn,
                    "site_id" => {site_id: i32},
                    "local_id" => {local_id: i32},
                    "type" => {type_: Option<String>},
                    "subtype" => {subtype: Option<String>},
                    "name" => {name: Option<String>},
                    "name2" => {name2: Option<String>},
                    "entity_id" => {entity_id: Option<i32>},
                    "worship_hfid" => {worship_hfid: Option<i32>},
                    "deity_type" => {deity_type: Option<i32>},
                    "deity_id" => {deity_id: Option<i32>},
                    "religion_id" => {religion_id: Option<i32>},
                    "dungeon_type" => {dungeon_type: Option<i32>},
                };},
            };
        } else {
            optional_filter! {
                query, id_filter,
                [
                    "local_id" => local_id,
                ],
                {group_by!{
                    group_by_opt, query, conn,
                    "site_id" => {site_id: i32},
                    "local_id" => {local_id: i32},
                    "type" => {type_: Option<String>},
                    "subtype" => {subtype: Option<String>},
                    "name" => {name: Option<String>},
                    "name2" => {name2: Option<String>},
                    "entity_id" => {entity_id: Option<i32>},
                    "worship_hfid" => {worship_hfid: Option<i32>},
                    "deity_type" => {deity_type: Option<i32>},
                    "deity_id" => {deity_id: Option<i32>},
                    "religion_id" => {religion_id: Option<i32>},
                    "dungeon_type" => {dungeon_type: Option<i32>},
                };},
            };
        }
    }
}

/// From Core to DB
impl Filler<Structure, df_st_core::Structure> for Structure {
    fn add_missing_data(&mut self, source: &df_st_core::Structure) {
        self.site_id.add_missing_data(&source.site_id);
        self.local_id.add_missing_data(&source.local_id);
        self.type_.add_missing_data(&source.type_);
        self.subtype.add_missing_data(&source.subtype);
        self.name.add_missing_data(&source.name);
        self.name2.add_missing_data(&source.name2);
        self.entity_id.add_missing_data(&source.entity_id);
        self.worship_hfid.add_missing_data(&source.worship_hfid);
        self.deity_type.add_missing_data(&source.deity_type);
        self.deity_id.add_missing_data(&source.deity_id);
        self.religion_id.add_missing_data(&source.religion_id);
        self.dungeon_type.add_missing_data(&source.dungeon_type);
    }
}

/// From DB to Core
impl Filler<df_st_core::Structure, Structure> for df_st_core::Structure {
    fn add_missing_data(&mut self, source: &Structure) {
        self.site_id.add_missing_data(&source.site_id);
        self.local_id.add_missing_data(&source.local_id);
        self.type_.add_missing_data(&source.type_);
        self.subtype.add_missing_data(&source.subtype);
        self.name.add_missing_data(&source.name);
        self.name2.add_missing_data(&source.name2);
        self.entity_id.add_missing_data(&source.entity_id);
        self.worship_hfid.add_missing_data(&source.worship_hfid);
        self.deity_type.add_missing_data(&source.deity_type);
        self.deity_id.add_missing_data(&source.deity_id);
        self.religion_id.add_missing_data(&source.religion_id);
        self.dungeon_type.add_missing_data(&source.dungeon_type);
    }
}

impl PartialEq<df_st_core::Structure> for Structure {
    fn eq(&self, other: &df_st_core::Structure) -> bool {
        self.site_id == other.site_id && self.local_id == other.local_id
    }
}

impl PartialEq<Structure> for df_st_core::Structure {
    fn eq(&self, other: &Structure) -> bool {
        self.site_id == other.site_id && self.local_id == other.local_id
    }
}

impl PartialEq<Structure> for Structure {
    fn eq(&self, other: &Self) -> bool {
        self.site_id == other.site_id && self.local_id == other.local_id
    }
}
