use df_st_core::{DeserializeBestEffort, DeserializeBestEffortTypes, Filler, HasUnknown};
use df_st_derive::{DeserializeBestEffort, HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{de, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(
    Serialize, DeserializeBestEffort, Clone, Debug, Default, HasUnknown, HashAndPartialEqById,
)]
pub struct WorldConstruction {
    // Could not find any legend.xml files that include
    // any <world_construction> tags
    pub id: i32,
    pub name: Option<String>,
    #[serde(alias = "type")]
    pub type_: Option<String>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, DeserializeBestEffort, Clone, Debug, Default, HasUnknown)]
pub struct WorldConstructions {
    pub world_construction: Option<Vec<WorldConstruction>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::WorldConstruction, WorldConstruction> for df_st_core::WorldConstruction {
    fn add_missing_data(&mut self, source: &WorldConstruction) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.type_.add_missing_data(&source.type_);
    }
}

impl PartialEq<df_st_core::WorldConstruction> for WorldConstruction {
    fn eq(&self, other: &df_st_core::WorldConstruction) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::WorldConstruction>, WorldConstructions>
    for Vec<df_st_core::WorldConstruction>
{
    fn add_missing_data(&mut self, source: &WorldConstructions) {
        self.add_missing_data(&source.world_construction);
    }
}

impl Filler<IndexMap<u64, df_st_core::WorldConstruction>, WorldConstructions>
    for IndexMap<u64, df_st_core::WorldConstruction>
{
    fn add_missing_data(&mut self, source: &WorldConstructions) {
        self.add_missing_data(&source.world_construction);
    }
}
