use df_st_core::{Filler, HasUnknown};
use df_st_derive::HasUnknown;
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct HERelationshipSupplement {
    // ID not given in xml, but later generated.
    // Might be added later to xml
    pub id: Option<i32>,
    pub event: Option<i32>,
    pub occasion_type: Option<i32>,
    pub site: Option<i32>,
    pub unk_1: Option<i32>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct HERelationshipSupplements {
    pub historical_event_relationship_supplement: Option<Vec<HERelationshipSupplement>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::HERelationshipSupplement, HERelationshipSupplement>
    for df_st_core::HERelationshipSupplement
{
    fn add_missing_data(&mut self, source: &HERelationshipSupplement) {
        if let Some(id) = source.id {
            // Override generated ID if ever added to output
            self.id.add_missing_data(&id);
        }
        self.event.add_missing_data(&source.event);
        self.occasion_type.add_missing_data(&source.occasion_type);
        self.site_id.add_missing_data(&source.site);
        self.unk_1.add_missing_data(&source.unk_1);
    }

    fn add_missing_data_indexed(&mut self, source: &HERelationshipSupplement, index: u64) {
        if source.id.is_none() {
            // If no ID set, use index
            self.id.add_missing_data(&(index as i32));
        }
        self.add_missing_data(source);
    }
}

impl PartialEq<df_st_core::HERelationshipSupplement> for HERelationshipSupplement {
    fn eq(&self, other: &df_st_core::HERelationshipSupplement) -> bool {
        self.event == other.event
            && self.occasion_type == other.occasion_type
            && self.site == other.site_id
            && self.unk_1 == other.unk_1
    }
}

impl std::hash::Hash for HERelationshipSupplement {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.event.hash(state);
        self.occasion_type.hash(state);
        self.site.hash(state);
        self.unk_1.hash(state);
    }
}

impl Filler<Vec<df_st_core::HERelationshipSupplement>, HERelationshipSupplements>
    for Vec<df_st_core::HERelationshipSupplement>
{
    fn add_missing_data(&mut self, source: &HERelationshipSupplements) {
        self.add_missing_data(&source.historical_event_relationship_supplement);
    }
}

impl Filler<IndexMap<u64, df_st_core::HERelationshipSupplement>, HERelationshipSupplements>
    for IndexMap<u64, df_st_core::HERelationshipSupplement>
{
    fn add_missing_data(&mut self, source: &HERelationshipSupplements) {
        self.add_missing_data(&source.historical_event_relationship_supplement);
    }
}
