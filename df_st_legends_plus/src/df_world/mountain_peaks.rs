use crate::deserializers::coordinate_deserializer;
use df_st_core::{Coordinate, Filler, HasUnknown};
use df_st_derive::{HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown, HashAndPartialEqById)]
pub struct MountainPeak {
    pub id: i32,
    pub name: Option<String>,
    #[serde(deserialize_with = "coordinate_deserializer")]
    #[serde(default)]
    pub coords: Option<Coordinate>,
    pub height: Option<i32>,
    pub is_volcano: Option<String>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct MountainPeaks {
    pub mountain_peak: Option<Vec<MountainPeak>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::MountainPeak, MountainPeak> for df_st_core::MountainPeak {
    fn add_missing_data(&mut self, source: &MountainPeak) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.coord.add_missing_data(&source.coords);
        self.height.add_missing_data(&source.height);
        self.is_volcano.add_missing_data(&source.is_volcano);
    }
}

impl PartialEq<df_st_core::MountainPeak> for MountainPeak {
    fn eq(&self, other: &df_st_core::MountainPeak) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::MountainPeak>, MountainPeaks> for Vec<df_st_core::MountainPeak> {
    fn add_missing_data(&mut self, source: &MountainPeaks) {
        self.add_missing_data(&source.mountain_peak);
    }
}

impl Filler<IndexMap<u64, df_st_core::MountainPeak>, MountainPeaks>
    for IndexMap<u64, df_st_core::MountainPeak>
{
    fn add_missing_data(&mut self, source: &MountainPeaks) {
        self.add_missing_data(&source.mountain_peak);
    }
}
