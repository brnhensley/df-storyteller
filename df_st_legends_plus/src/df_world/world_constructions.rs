use crate::deserializers::coordinates_deserializer;
use df_st_core::{Coordinate, Filler, HasUnknown};
use df_st_derive::{HasUnknown, HashAndPartialEqById};
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown, HashAndPartialEqById)]
pub struct WorldConstruction {
    pub id: i32,
    pub name: Option<String>,
    #[serde(alias = "type")]
    pub type_: Option<String>,
    #[serde(deserialize_with = "coordinates_deserializer")]
    #[serde(default)]
    pub coords: Option<Vec<Coordinate>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, HasUnknown)]
pub struct WorldConstructions {
    pub world_construction: Option<Vec<WorldConstruction>>,

    #[serde(flatten)]
    pub unknown: HashMap<String, Value>,
}

impl Filler<df_st_core::WorldConstruction, WorldConstruction> for df_st_core::WorldConstruction {
    fn add_missing_data(&mut self, source: &WorldConstruction) {
        self.id.add_missing_data(&source.id);
        self.name.add_missing_data(&source.name);
        self.type_.add_missing_data(&source.type_);
        self.coords.add_missing_data(&source.coords);
    }
}

impl PartialEq<df_st_core::WorldConstruction> for WorldConstruction {
    fn eq(&self, other: &df_st_core::WorldConstruction) -> bool {
        self.id == other.id
    }
}

impl Filler<Vec<df_st_core::WorldConstruction>, WorldConstructions>
    for Vec<df_st_core::WorldConstruction>
{
    fn add_missing_data(&mut self, source: &WorldConstructions) {
        self.add_missing_data(&source.world_construction);
    }
}

impl Filler<IndexMap<u64, df_st_core::WorldConstruction>, WorldConstructions>
    for IndexMap<u64, df_st_core::WorldConstruction>
{
    fn add_missing_data(&mut self, source: &WorldConstructions) {
        self.add_missing_data(&source.world_construction);
    }
}
