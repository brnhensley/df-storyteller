use colored::*;
use indicatif::{MultiProgress, ProgressBar, ProgressStyle};
#[allow(unused_imports)]
use log::{debug, error, info, trace, warn};
use std::path::PathBuf;
use std::thread;
use std::thread::JoinHandle;

use df_st_core::config::RootConfig;
use df_st_core::*;
use df_st_db::*;
use df_st_legends::*;
use df_st_legends_plus::*;

mod find_files;
mod unknown_items;
use find_files::{
    find_missing_files, find_missing_images, find_site_map_images, get_file_prefix,
    get_world_info_from_filename,
};
use unknown_items::{print_unknown_elements_list, print_unknown_list_combined};

pub fn parse_xml_files(file: &PathBuf) -> DFWorld {
    // Create Core world
    let mut df_world_core: DFWorld = DFWorld::new();

    let filenames = find_missing_files(&file);
    if !filenames.some_found() {
        error!("Could not find any files. No legends file to parse.");
        return df_world_core;
    }

    info!("Parsing data ...");
    let m = MultiProgress::new();
    let sty = ProgressStyle::default_bar()
        .template(
            "{prefix:<18}: [{elapsed_precise}] {msg:>1}{spinner:.yellow} [{bar:50.green}] \
            {bytes}/{total_bytes} (eta: {eta})",
        )
        .progress_chars("=> ");

    let start = std::time::Instant::now();

    let mut legends_thread_option: Option<JoinHandle<thread::Result<DFWorldLegends>>> = None;
    if let Some(legends_file) = filenames.legends {
        let progress_bar = m.add(ProgressBar::new(100));
        progress_bar.set_style(sty.clone());
        progress_bar.set_prefix("legends.xml");
        legends_thread_option = Some(thread::spawn(move || -> thread::Result<DFWorldLegends> {
            // info!("Parsing legends.xml ...");
            let df_world_legends = parse_legends(&legends_file, progress_bar);
            // info!("Parsing legends.xml {}", "Done".green());
            Ok(df_world_legends)
        }));
    }

    let mut legends_plus_thread_option: Option<JoinHandle<thread::Result<DFWorldLegendsPlus>>> =
        None;
    if let Some(legends_plus_file) = filenames.legends_plus {
        let progress_bar = m.add(ProgressBar::new(100));
        progress_bar.set_style(sty.clone());
        progress_bar.set_prefix("legends_plus.xml");
        legends_plus_thread_option = Some(thread::spawn(
            move || -> thread::Result<DFWorldLegendsPlus> {
                // info!("Parsing legends_plus.xml ...");
                let df_world_legends_plus = parse_legends_plus(&legends_plus_file, progress_bar);
                // info!("Parsing legends_plus.xml {}", "Done".green());
                Ok(df_world_legends_plus)
            },
        ));
    }

    // Parse other files on main thread
    // println!("\tParsing world_history.txt ...");
    // parse_world_history(&format!("../world/{}-world_history.txt",filenameprefix));
    // println!("\tParsing world_history.txt {}", "Done".green());
    // println!("\tParsing world_sites_and_pops.txt ...");
    // parse_world_site_and_pops(&format!("../world/{}-world_sites_and_pops.txt",filenameprefix));
    // println!("\tParsing world_sites_and_pops.txt {}", "Done".green());

    // merge back everything to main thread
    let mut df_world_legends: DFWorldLegends = DFWorldLegends::new();
    let mut df_world_legends_plus: DFWorldLegendsPlus = DFWorldLegendsPlus::new();
    m.join().unwrap();
    if let Some(legends_thread) = legends_thread_option {
        df_world_legends = legends_thread.join().unwrap().unwrap();
    }
    if let Some(legends_plus_thread) = legends_plus_thread_option {
        df_world_legends_plus = legends_plus_thread.join().unwrap().unwrap();
    }

    // Check if there are tags that are not implemented
    print_unknown_elements_list(&df_world_legends, "legends.xml");
    print_unknown_list_combined(&df_world_legends, "legends.xml");
    print_unknown_elements_list(&df_world_legends_plus, "legends_plus.xml");
    print_unknown_list_combined(&df_world_legends_plus, "legends_plus.xml");

    // println!("{:#?}", df_world_legends);
    // println!("{:#?}", df_world_legends_plus.mountain_peaks);
    info!("Parsing data {}", "Done".green());
    info!("Parsing duration: {:?}", start.elapsed());

    info!("Merging data ...");
    let start = std::time::Instant::now();
    // Add data to Core world
    df_world_core.add_missing_data(&df_world_legends);
    // Try to save a bit of memory
    drop(df_world_legends);
    df_world_core.add_missing_data(&df_world_legends_plus);
    info!("Merging data {}", "Done".green());
    info!("Merging duration: {:?}", start.elapsed());

    info!(
        "Combined items include:\n{}",
        df_world_core.list_containt_counts()
    );
    df_world_core
}

pub fn parse_and_store_xml(file: &PathBuf, config: &RootConfig, world_id: i32) {
    let pool = df_st_db::establish_connection(&config).expect("Failed to connect to database.");
    let conn = pool.get().expect("Couldn't get db connection from pool.");

    // TODO check if world id is used, if already used display this.
    let filename = get_file_prefix(&file);
    let mut df_world = parse_xml_files(&file);
    // trace!("{:#?}", df_world);

    // Add missing ID's
    info!("Create missing ID's ...");
    let start = std::time::Instant::now();
    df_world = df_world.add_missing_ids();
    info!("Create missing ID's {}", "Done".green());
    info!("Create missing ID's duration: {:?}", start.elapsed());

    info!("Storing data ...");
    let start = std::time::Instant::now();
    // store in db objects
    let mut df_world_db: DBDFWorld = DBDFWorld::new();
    // Add data to DB world
    df_world_db.add_missing_data(&df_world);
    // Add additional info from filename
    if let Some(filename) = filename {
        let df_world_info = get_world_info_from_filename(&filename, world_id);
        df_world_db.world_info.add_missing_data(&df_world_info);
    }
    // Set world_id for all items
    df_world_db.set_world_id(world_id);
    // println!("{:#?}", df_world_db);
    let progress_bar = ProgressBar::new(100);
    progress_bar.set_style(ProgressStyle::default_spinner().template(
        "{prefix:<18}: [{elapsed_precise}] [{bar:40.green}] {pos:>2}/{len:2} \
        (eta: {eta}) {wide_msg}",
    ));
    progress_bar.set_prefix("Storing in DB");

    // store db objects
    df_world_db.insert_into_db(&conn, Some(progress_bar));

    info!("Storing data {}", "Done".green());
    info!("Storing data duration: {:?}", start.elapsed());
}

pub fn parse_world_map_images(file: &PathBuf, config: &RootConfig, world_id: i32) {
    let pool = df_st_db::establish_connection(&config).expect("Failed to connect to database.");
    let conn = pool.get().expect("Couldn't get db connection from pool.");

    info!("Loading map images ...");
    let file_names = find_missing_images(&file);
    if !file_names.some_found() {
        warn!("Could not find any files. No world map images to parse.");
        return;
    }
    let world_images = df_st_image_maps::parse_world_map_images(file_names);
    info!("Loading map images {}", "Done".green());

    info!("Storing data ...");
    info!("NOTE: The `Historical Events` usually take a bit longer to store (there are just more of them).");
    let start = std::time::Instant::now();
    // store in db objects
    let mut world_images_db: df_st_db::WorldMapImages = df_st_db::WorldMapImages::new();
    // Add data to DB world
    world_images_db.add_missing_data(&world_images);
    // Set world_id for all items
    world_images_db.set_world_id(world_id);
    // println!("{:#?}", df_world_db);
    let progress_bar = ProgressBar::new(100);
    progress_bar.set_style(ProgressStyle::default_spinner().template(
        "{prefix:<18}: [{elapsed_precise}] [{bar:40.green}] {pos:>2}/{len:2} \
        (eta: {eta}) {wide_msg}",
    ));
    progress_bar.set_prefix("Storing in DB");

    // store db objects
    world_images_db.insert_into_db(&conn, progress_bar);

    info!("Storing data {}", "Done".green());
    info!("Storing data duration: {:?}", start.elapsed());
}

pub fn parse_site_map_images(file: &PathBuf, config: &RootConfig, world_id: i32) {
    let pool = df_st_db::establish_connection(&config).expect("Failed to connect to database.");
    let conn = pool.get().expect("Couldn't get db connection from pool.");

    info!("Loading site map images ...");
    let file_names = find_site_map_images(&file);
    if !file_names.some_found() {
        warn!("Could not find any files. No site map images to parse.");
        return;
    }
    let site_map_images = df_st_image_site_maps::parse_site_map_images(file_names);
    info!("Loading site map images {}", "Done".green());

    info!("Storing data ...");
    let start = std::time::Instant::now();
    // store in db objects
    let mut site_map_images_db: df_st_db::SiteMapImages = df_st_db::SiteMapImages::new();
    // Add data to DB world
    site_map_images_db.add_missing_data(&site_map_images);
    // Set world_id for all items
    site_map_images_db.set_world_id(world_id);
    // println!("{:#?}", df_world_db);
    let progress_bar = ProgressBar::new(100);
    progress_bar.set_style(ProgressStyle::default_spinner().template(
        "{prefix:<18}: [{elapsed_precise}] [{bar:40.green}] {pos:>2}/{len:2} \
        (eta: {eta}) {wide_msg}",
    ));
    progress_bar.set_prefix("Storing in DB");

    // store db objects
    site_map_images_db.insert_into_db(&conn, progress_bar);

    info!("Storing data {}", "Done".green());
    info!("Storing data duration: {:?}", start.elapsed());
}
