# Become a painter

Do you want to visualize legends data yourself! You are in the right place.
Here you find some of out finest sketches. There is something for everyone,
take a look around. These sketches should help you get started.
And you will be a legendary painter in no time!

You can find out assortment of [sketches here](https://gitlab.com/df_storyteller/df-storyteller-sketches).

If you like to show of your own creations or just want to browse, take a look
at our [gallery](paintings.md). If you want to create a canvas using you
preferred materials please share it, so we can add it to the list above.

## Things you need to know
If you want to become a painter note the following things as they will effect you.

### Backwards compatibility
We will try to support most of the versions of Dwarf Fortress in future versions of the 
application but we might need to have breaking changes at some point to further the 
development of the application. For more information see [this page](docs/versions.md).

For changes you can also look at the [application changes](CHANGELOG.md)
and [api changes](CHANGELOG_API.md).

### DF Version
All of the API calls have version numbers attached to them this way you can see
what version of DF Storyteller people will need to run your application.
Just keep track of the highest version number.
Because of our [update policy](docs/versions.md) you don't have to worry about this to
much as we will force users to update after a year.

### DF Storyteller Settings
Users can change different [settings](docs/settings.md) in there application that should
directly effect the UI. These settings and preferences can be acquired using the `/api/df_st_info` 
API call. The `base_url` can be acquired here, of course the first API url has to be provided somehow. 
For this you should allow some way for the user to input this url at the start.
We will show the base URL to the user in the terminal so you can refer to this to the user.

### Pre version 1.0.0
We are still developing and updating our API and application so things might change quickly.
This means that we will change API output and maybe even API calls at any point.
We want to make our API intuitive to new painters and thus responses might change name.
We are planning to make out API more stable in release `1.0.0`. Before this your painting might
break frequently, sorry. But to make it up to you we will document all our (breaking) changes
to our API in our [`CHANGELOG_API.md` file](CHANGELOG_API.md).

### Language and Technologies
Because RESTful APIs are supported almost everywhere these days you have a lot
of choice what language and technologies to use. But we have some
suggestion and remarks you might want to take into account.

Because DF Storyteller has build in support for the hosting of files through the
"serve-paintings" folder. And because of a lot of other reasons we **highly**
recommend you use standard web technologies and/or languages.

The best choice is to use web technologies like 
[HTML](https://developer.mozilla.org/en-US/docs/Web/HTML), 
[CSS](https://developer.mozilla.org/en-US/docs/Web/CSS) and 
[JS](https://developer.mozilla.org/en-US/docs/Web/JavaScript) or even 
[Web Assembly](https://developer.mozilla.org/en-US/docs/WebAssembly).
These are easy to use, both for the developer and user and are more then
capable of almost everything you need.

But if you really want to use something else, go ahead. We all like to experiment sometimes.
Here are some popular options and some advice/comments regarding the creation of visualizers,
(but meanings differ[^1]:
* Rust: Best to compile to Web Assembly, you have the power of Rust and
the flexibility of the web.
* Node.js: Just compile it to JS and run it in the browser or
compile to Web Assembly. Both are better options then making the user
install node.js.
* Python: A very common beginners language, but for DF Storyteller we recommend JS as a beginners language.
* Korlin: Have not used it, but seems popular.
* Go: Popular but have not used it.
* C#: Not great for cross platform support, in most cases, but can be cross platform.
* Java: JVM is installed by a lot of users these days, but a bit heavy in
most cases here. Also UI might not be the best.
* C++: Just please learn Rust, you don't want to start another C++ project these days, al least not for this, unless you REALLY want to.
* ...

**Mobile development**: We still recommend using web technologies, but native applications
could also work. This can even work withouts needing to have DF ST working on the mobile device.
This is because to can run DF ST on your computer and just connect to it over LAN.
You can not use it on the go that way, but it can be used until DF ST is working on
mobile devices.

[^1]: You might have different opinions then I do, and that is good.
If you want to use a language, please do! I just want to help you make the right decision.

**PHP**: In the past PHP was a popular language for creating websites.
But because of the client side nature we do **NOT** suggest you 
use it to create visualizers. For more info and discussions look
[here](https://gitlab.com/df_storyteller/df-storyteller/issues/64).

## Technical info
For more technical info [look here](docs/technical.md).