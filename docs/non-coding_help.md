# Contributing for non-programmers

You can help us a lot too. How you ask? (They are listed in proximate increasing
difficulty)

* **Report bugs**: If you use our program you might have spotted to something
that is not working correctly. For this you can open a
[new issue](https://gitlab.com/df_storyteller/df-storyteller/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).
This way we know what to fix. You can also look through
[the list](https://gitlab.com/df_storyteller/df-storyteller/-/issues) and see if
someone already found the issue and add you comments to it.

* **Help us resolve issues**: When people report an issue we might need more
information about the bug. This is usually denoted with the following labels:
`Need Confirmation` or `More info needed`. You can view these
[issues here](https://gitlab.com/df_storyteller/df-storyteller/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=More%20info%20needed&label_name[]=Need%20Confirmation).

* **Fix an issue**: Some issues are labeled with
[Beginner-non-coder](https://gitlab.com/df_storyteller/df-storyteller/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Beginner-non-coder).
These issue are usually a good place to start looking.

* **Fix/improve documents**: We have documents like these and there are very
likely many improvements we can make. Not to speak of all the spelling mistakes
we are making. (not everyone is a native English speaker) You can edit these
documents strait in GitLab with the 'Web IDE' or use the 'Edit' button. Most of
the documents are written in [markdown](https://www.markdownguide.org/basic-syntax/),
it is not complicated, I promise. It is the same as Discord. (There are also
editors for this, I'm using [Typora](https://typora.io/) to write this.)

  * When you finish editing the document, type a 'commit message' that described
  what you have done. These are usually 1 short sentence for example "Fixed
  various typos and added typos in README.md" then 2 enter/new lines/empty line.
  And a longer description "There were various mistakes. Added a small section
  about how communication methods.". The final result should look something like this:

    ```
    Fixed various typos and added typos in README.md

    There were various mistakes.
    Added a small section about how communication methods.
    ```

  * In the 'Target Branch' you write a small name like `fixed-readme-typos` and
  leave the check-box 'Start a **new merge request** with these changes' checked.

* **Fix/improve documentation**: This is a bit more tricky because documentation
is inside the files with the code. But you don't have to know any coding to do
this. Most of the documentation is in the `df_st_core/src/` folder. So for
example I know from [an issue](https://gitlab.com/df_storyteller/df-storyteller/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Documentation)
that there is a mistake in the documentation of `Region`. I can now go to
`df_st_core/src/df_world/region.rs` here I can search for the text around the issue.

  * I found these lines:
  ```rust
  /// A Region is a section of map that has specific properties.
  /// These regions are usualy described by natural features likes forest, oceans or lakes.
  /// Every region tile should be in at least one `Region`.
  /// [`Sites`](crate::df_world::site::Site) are thus always in at least one `Region`.
  #[derive(Serialize, Deserialize, Clone, Debug, ... )]
  pub struct Region { ... }
  ```
  The `///` means that it is documentation for the lines of code below
  (`struct Region` in this case). `//!` means that is documentation for the
  line above or for the whole file. Editing this documentation will change the
  documentation in all other documentation, code documentation, API, ... The
  rest is similar to the steps in "Fix/improve documents".

* **Style and design**: Maybe you are an artist or just like to design things.
Well you can always make designs for our website, documentation, logo or
[help one of our painters](paintings.md) to improve there design. (We can of
course not promise that we will change our logo or designs as this might take
a lot of time and we are not changing logo every week 😅, But we are probably
not designers, so can still use your help). Contact us on
[Discord](https://discord.gg/aAXt6uu) to be sure.

* ... There are a bunch more. In general, just lets us know somewhere what you have
in mind and we can guild you. Contact us on [Discord](https://discord.gg/aAXt6uu)
or via a [GitLab Issue](https://gitlab.com/df_storyteller/df-storyteller/-/issues).
